from random import randint
from models.alexa import PlainAlexaResponse


INSULT_INTENTS = ['Personality', 'Appearance']


def looks_intent():
    insult = randint(1, 2)
    if insult == 1:
        jab = 'How many times do I have to flush before you go away?'
    else:
        jab = "Don't kid yourself."
    response = PlainAlexaResponse('Insult', jab)
    return response.get_statement()


def personality_intent():
    insult = randint(1, 10)
    if insult == 1:
        jab = 'I bet that attitude of yours was really cool back in high school.'
    elif insult == 2:
        jab = 'I am TRYING to imagine you with a personality, but nothing is coming to mind.'
    elif insult == 3:
        jab = 'You really are a deranged, psychotic, derelict.'
    elif insult == 4:
        jab = 'You are a real asset to humanity. Well, forget the "set" part.'
    elif insult == 5:
        jab = "I didn't think the clowns at the circus actually talked."
    elif insult == 6:
        jab = "You'er not that great"
    elif insult == 7:
        jab = 'Are you ALWAYS such a ray of sunshine or is today really special?'
    elif insult == 8:
        jab = 'I don’t hate you. I don’t care enough about you to hate you.'
    elif insult == 9:
        jab = "You certainly can't be a good example, but you'd make a terrible warning."
    elif insult == 10:
        jab = 'Please shut your mouth when you’re talking to me.'
    else:
        jab = 'I have better things to do, thank you.'
    response = PlainAlexaResponse('Insult', jab)
    return response.get_statement()


def insult_intent(event, context):
    intent = event['request']['intent']['name']
    if intent == 'Personality':
        return personality_intent()
    if intent == 'Appearance':
        return looks_intent()
