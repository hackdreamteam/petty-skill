"""
Contains all objects used by the dining interface
"""
import urllib
from configparser import ConfigParser
import os
import pymongo


class Restaurant:
    """
    Represents the Restaurants found on campus. Includes the name, hours, payment types, menu, and phone number of the
    restaurant
    """
    __slots__ = ['name', 'hours', 'accepted_payments', 'menu', 'phone_number']

    def __init__(self, name=None, hours=None, accepted_payments=None, menu=None, phone_number=None, dic=None):
        if dic is not None:
            self.from_dic(dic)
        else:
            self.name = name
            self.hours = hours
            self.accepted_payments = accepted_payments
            self.menu = menu
            self.phone_number = phone_number

    def from_dic(self, dic):
        self.name = dic['name']
        print(self.name)
        self.hours = dic['hours']
        self.accepted_payments = dic['accepted_payments']
        self.menu = dic['menu']
        self.phone_number = dic['phone_number']

    def as_dict(self):
        result = dict()
        result['name'] = self.name
        result['hours'] = self.hours
        result['accepted_payments'] = self.accepted_payments
        result['menu'] = self.menu
        result['phone_number'] = self.phone_number
        return result

    def __str__(self):
        return 'Restaurant: ' + self.name


class Database:
    __slots__ = ['client', 'config']

    def get_config(self):
        current_path = os.path.abspath(os.path.dirname(__file__))
        property_file = os.path.join(current_path, 'database.ini')
        config = ConfigParser()
        config.read(property_file)
        return config

    def __init__(self):
        self.config = self.get_config()
        username = urllib.parse.quote_plus(self.config['user']['username'])
        password = self.config['user']['password']
        cluster_url = self.config['cluster']['url']
        url = 'mongodb://' + username + ':' + password + cluster_url
        self.client = pymongo.MongoClient(url).get_database(self.config['database']['name'])