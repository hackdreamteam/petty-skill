from models.alexa import PlainAlexaResponse
from homemaking.homemaking import homemaking_intent, HOMEMAKING_INTENTS
from insults.insults import insult_intent, INSULT_INTENTS
from dining.dining_intent import dining_intent, DINING_INTENTS


def lambda_handler(event, context):
    intent = event['request']['intent']['name']
    if intent in HOMEMAKING_INTENTS:
        return homemaking_intent(event, context)
    if intent in INSULT_INTENTS:
        return insult_intent(event, context)
    if intent in DINING_INTENTS:
        return dining_intent(event)
    alexa_response = PlainAlexaResponse("Tile", "Body")
    return alexa_response.get_statement()


def test():
    # Homemaker tests
    event = {
        'request': {
            'intent': {
                'name': 'Microwave',
                'slots': {
                    'name': 'College Grind',
                    'menu_item': 'bagel'
                }
            }
        }
    }
    print(lambda_handler(event, None))
    print('-------------------------')
    event['request']['intent']['name'] = 'Laundry'
    print(lambda_handler(event, None))
    print('-------------------------')
    event['request']['intent']['name'] = 'Vacuuming'
    print(lambda_handler(event, None))

    # Insults tests
    event['request']['intent']['name'] = 'Personality'
    print(lambda_handler(event, None))
    print('-------------------------')
    event['request']['intent']['name'] = 'Appearance'
    print(lambda_handler(event, None))

    # Dining tests
    event['request']['intent']['name'] = 'Hours'
    print(lambda_handler(event, None))
    print('-------------------------')
    event['request']['intent']['name'] = 'On Menu'
    print(lambda_handler(event, None))
    print('-------------------------')
    event['request']['intent']['name'] = 'Get Payments'
    print(lambda_handler(event, None))


if __name__ == '__main__':
    test()
