from models.alexa import PlainAlexaResponse
from configparser import ConfigParser
import os

HOMEMAKING_INTENTS = ['Microwave', 'Laundry', 'Vacuuming']


def get_config():
    config_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'homemaking.ini')
    config = ConfigParser()
    config.read(config_file)
    return config


def vacuuming_intent(config):
    instructions = config['vacuuming']['instructions']
    response = PlainAlexaResponse('Vacuuming Instructions', instructions)
    return response.get_statement()


def dishes_intent(config):
    instructions = config['dishes']['instructions']
    response = PlainAlexaResponse('Dished Instructions', instructions)
    return response.get_statement()


def laundry_intent(config):
    instructions = config['laundry']['instructions']
    response = PlainAlexaResponse('Laundry Instructions', instructions)
    return response.get_statement()


def microwave_intent(intent, config):
    # foods = intent['slots']['slotKey']['value']
    # if foods == "Hot Pockets":
        pass


def homemaking_intent(event, context):
    config = get_config()

    intent = event['request']['intent']['name']

    if intent == 'Microwave':
        return microwave_intent(intent, config)

    if intent == 'Laundry':
        return laundry_intent(config)

    if intent == 'Vacuuming':
        return vacuuming_intent(config)
