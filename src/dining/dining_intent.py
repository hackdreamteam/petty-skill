from models.dining import Database
from models.alexa import PlainAlexaResponse

DINING_INTENTS = ['Hours', 'On Menu', 'Phone Number', 'Get Payments']
database = Database()


def get_by_name(name):
    restaurants = database.client.get_collection('dining').find({})
    for restaurant in restaurants:
        if name.lower() in restaurant['name'].lower():
            return restaurant


def hour_intent(name):
    restaurant = get_by_name(name)
    hours = restaurant['hours'].strip()
    if 'closed' in hours.lower():
        message = name + ' is closed'
    else:
        message = name + ' is open from ' + hours
    response = PlainAlexaResponse('Hour', message)
    return response.get_statement()


def menu_intent(name, item):
    restaurant = get_by_name(name)

    for food in restaurant['menu']:
        if item.lower() in food.lower() or food.lower() in item.lower():
            message = name + ' has ' + item
            response = PlainAlexaResponse('Menu', message)
            return response.get_statement()
    message = name + ' does not have ' + item
    response = PlainAlexaResponse('Menu', message)
    return response.get_statement()


def phone_intent(name):
    phone_number = get_by_name(name)['phone_number']
    message = 'The phone number for ' + name + ' is ' + phone_number
    response = PlainAlexaResponse('Phone', message)
    return response.get_statement()


def accepted_payment_intent(name):
    payments = get_by_name(name)['accepted_payments']
    message = name + ' accepts '
    for i in range(0, len(payments)-1):
        message += payments[i] + ', '
    message += 'and ' + payments[len(payments)-1]
    response = PlainAlexaResponse('Payment', message)
    return response.get_statement()


def dining_intent(event):
    intent = event['request']['intent']['name']
    name = event['request']['intent']['slots']['name']
    if intent == DINING_INTENTS[0]:
        return hour_intent(name)
    if intent == DINING_INTENTS[1]:
        menu_item = event['request']['intent']['slots']['menu_item']
        return menu_intent(name, menu_item)
    if intent == DINING_INTENTS[2]:
        return phone_intent(name)
    if intent == DINING_INTENTS[3]:
        return accepted_payment_intent(name)


def test():
    print(hour_intent('RITz'))
    print(menu_intent('College Grind', 'bagels'))
    print(accepted_payment_intent('RITz'))


if __name__ == '__main__':
    test()

