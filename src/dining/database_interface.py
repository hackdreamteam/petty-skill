"""
Handles all interaction with the mongodb. Values are paseed into this function that should be saved into the database.
@author Collin Bolles
"""
import pymongo
import urllib
from configparser import ConfigParser
import os


def get_database_config():
    """
    Get the configuration details on the database from the properties file located in the same directory
    :return: ConfigParser object
    """
    current_path = os.path.abspath(os.path.dirname(__file__))
    property_file = os.path.join(current_path, 'database.ini')
    config = ConfigParser()
    config.read(property_file)
    return config


def get_db():
    """
    Get the pymongo database interface object
    :return: MongoClient
    """
    config = get_database_config()
    username = urllib.parse.quote_plus(config['user']['username'])
    password = config['user']['password']
    cluster_url = config['cluster']['url']
    url = 'mongodb://' + username + ':' + password + cluster_url
    return pymongo.MongoClient(url).get_database(config['database']['name'])


def get_restaurants():
    """
    Handles saving the array of restaurants passed into the function
    :param restaurants: List of Restaurant objects
    :return: None
    """
    client = get_db()
    dining = client.get_collection(get_database_config()['database']['collection_dining'])
    restaurants = []
    for value in dining.find({}):
        restaurants.append(value)
    print(restaurants)


def test():
    get_restaurants()


if __name__ == '__main__':
    test()